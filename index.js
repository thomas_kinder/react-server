var fs = require('fs');
var http = require('http');
var https = require('https');
var certificate = fs.readFileSync("/opt/il/letsencrypt/fullchain.pem");
var privateKey = fs.readFileSync("/opt/il/letsencrypt/privkey.pem");
var dbPath = "/opt/il/db.json";

//password of the user is user is passed on the server with command line arguments
var user = { password: "AdiYogi112", name: "ipc@isha" };
var user1 = {password: "AdiYogi112", name: "isha@usa"};

var db = null;

try {
	db = JSON.parse(fs.readFileSync(dbPath));

} catch (error) {
	console.log("could not read database:: ", error);
}

var credentials = { key: privateKey, cert: certificate };
var express = require('express');
var cors = require('cors');
var app = express();

app.use(cors())

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();

const { forceDomain } = require('forcedomain');
app.use(forceDomain({
	hostname: 'www.ishalive.de',
	protocol: 'https'
}));


var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

// httpServer.listen(8080);
httpsServer.listen(443);
httpServer.listen(80);

//app.use(express.static("/opt/il/ieco-proctor-test-view/build"));
//app.use(express.static("/opt/il/ieco-rtc-client/build"));
//app.use(express.static("/opt/il/ieco-rtc-client-staging/build"));
//app.use(express.static("/opt/il/ieco-rtc-client-staging-old/build"));
app.use(express.static("/opt/il/iecso-jwstream-checker/build"));
//app.use(express.static("/opt/il/misc"));
app.use(express.static("/opt/il/isha-live-2.0/build"));


/*app.get('/staging-old', function (req, res) {
	res.sendFile('/opt/il/ieco-rtc-client-staging-old/build/index.html');
});
*/

app.get('/sg', function (req, res) {
        res.sendFile('/opt/il/isha-live-2.0/build/index.html');
});
/*
app.get('/staging', function (req, res) {
	res.sendFile('/opt/il/ieco-rtc-client-staging/build/index.html');
});
*/

app.get('/stream-checker', function (req, res) {
	res.sendFile('/opt/il/iecso-jwstream-checker/build/index.html');
});

/*
app.get('/', function (req, res) {
	console.log("main route was hit");
	res.sendFile(path.join('/opt/il/ieco-rtc-client/build/index.html'));
	//res.sendFile(path.join('/opt/il/ieco-proctor-test-view/build/index.html'));
});
*/


var db;

try {
	var fs = require('fs')
	var db = JSON.parse(fs.readFileSync(dbPath));
} catch (e) {
	console.log(e);
}


var _getRequestRoot = (req, res) => {
	console.log(req.body);
	try {
		console.log("email = ", req.body.email, db[req.body.email.trim().toLowerCase()]);
		let userInfo = db[req.body.email.trim().toLowerCase()];
		if (userInfo) {
			res.json(userInfo);
		}

		res.status(404).send('Not found');

	} catch (e) { console.log(e) }
};


var setDB = (req, res) => {
	console.log("boday.....", req.body);
	fs.writeFileSync(dbPath, JSON.stringify(req.body))
}


var simpleLogin = (req, res) => {
	try {//first try db,
		console.log("email = ", req.body.email, db[req.body.email]);
		let userInfo = db[req.body.email.trim().toLowerCase()];

		if (userInfo) {
			res.json(userInfo);
			return;
		}

		throw "no email in db";


	} catch {//if db was not successful try simple login string so we can loginto any room with any role


		try {
			console.log(req.body.email)
			let user = req.body.email.split("_");


			let localRemote = user[1] === "l" ? "https://localhost:" : "https://iecso.isha.in:"//"https://qa-client.ieco.isha.in:";
			let finalPort = 4000 + parseInt(user[2]);
			let serverNo = localRemote + finalPort;

			let msg = {
				serverNo: serverNo,
				userId: req.body.email,
				role: user[0],
				roomNo: user[3],
				seatNo: user[4],
				classCfg: [
					{ port: "4000", groupId: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] },
					{ port: "4001", groupId: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] }
				]
			}

			res.json(msg);

		} catch (error) {
			console.log("simpleLogin: error: ", error);
			res.status(404).send('Not found');
		}
	}
}

const verifyUser = (password, name) => {
	if (name === user.name && password ===  user.password){
		return true;
	}

	if (name === user1.name && password ===  user1.password){
		return true;
	}

	return false;
}


const streamData = (req, res) => {
	console.log("stram data post call");
	console.log("request data = ", req.body);

	if (verifyUser(req.body.password, req.body.name)) {
		//ok return the data
		console.log("user authenticated");
		const channelMap = [
			[
				{ prim: "FX0AyupQ", sec: "aWbMRg32" },
				{ prim: "YncRl6ip", sec: "z88VYZZQ" },
				{ prim: "XRL0vOmg", sec: "O84g3HWy" },
				{ prim: "BMl2VJE8", sec: "7KIp9Btv" },
				{ prim: "y5JgCtmQ", sec: "KvEfLpqI" },
			],
			[
				{ prim: "6W9OviAW", sec: "ZFCT1ZY6" },
				{ prim: "t6lq9Ykn", sec: "HFyU43t5" },
				{ prim: "F17UGonk", sec: "xnOoT1ZM" },
				{ prim: "XvwXiWbn", sec: "sIXoVEW4" },
				{ prim: "3iZ82Zth", sec: "L5qxMWrV" },
			],
			[
				{ prim: "3OqMhEYT", sec: "IcZMrggA" },
				{ prim: "JZI2ad2i", sec: "ZG0sMLpb" },
				{ prim: "uwpAxweE", sec: "3AqSIu9L" },
				{ prim: "gWQR3rl4", sec: "XqLRmTDL" },
				{ prim: "AqU9tDh2", sec: "1kJuNggz" },
			],
			[
				{ prim: "FX0AyupQ", sec: "aWbMRg32" },
				{ prim: "YncRl6ip", sec: "z88VYZZQ" },
				{ prim: "XRL0vOmg", sec: "O84g3HWy" },
				{ prim: "BMl2VJE8", sec: "7KIp9Btv" },
				{ prim: "y5JgCtmQ", sec: "KvEfLpqI" },
			],
			[
				{ prim: "6W9OviAW", sec: "ZFCT1ZY6" },
				{ prim: "t6lq9Ykn", sec: "HFyU43t5" },
				{ prim: "F17UGonk", sec: "xnOoT1ZM" },
				{ prim: "XvwXiWbn", sec: "sIXoVEW4" },
				{ prim: "3iZ82Zth", sec: "L5qxMWrV" },
			],
			[
				{ prim: "3OqMhEYT", sec: "IcZMrggA" },
				{ prim: "JZI2ad2i", sec: "ZG0sMLpb" },
				{ prim: "uwpAxweE", sec: "3AqSIu9L" },
				{ prim: "gWQR3rl4", sec: "XqLRmTDL" },
				{ prim: "AqU9tDh2", sec: "1kJuNggz" },
			],
			[
				{ prim: "zF3JCXmo", sec: "XFFN3pMJ" },
				{ prim: "dVt2dpOK", sec: "CGQHfvUt" },
			]
		]
		
		res.json({channelMap: channelMap});
		return;
	}

	res.status(404).send('Not found');
}

app.post('/email', jsonParser, _getRequestRoot);
app.post('/streamdata', jsonParser, streamData)
app.post('/setDb', jsonParser, setDB);
app.post('/simplelogin', jsonParser, simpleLogin);

console.log("running on port 443");


